//Library-----------------------------------

const Scene = require('Scene');
const Time = require('Time');
export const Diagnostics = require('Diagnostics');
const Textures = require('Textures');
const Materials = require('Materials');
const CameraInfo = require('CameraInfo');
const FaceGestures = require('FaceGestures');
const FaceTracking = require('FaceTracking');
const Patches = require('Patches');
const Random = require('Random');
const Audio = require('Audio');
const Reactive = require('Reactive');

//-----------------------------------Library

(async function() {

    //ToScriptVariables--------------------------------

    let isRecording = await Patches.outputs.getBoolean('isRecording');
    let RecordingTime = await Patches.outputs.getScalar('RecordingTime');
    
    //--------------------------------ToScriptVariables

    Promise.all([

        
        //GetSceneObjects--------------------------------

        ]).then(function(results){

        //--------------------------------GetSceneObjects

        //variables--------------------------------
        var CheetosQuestions = [0,1];
        var CheetosQuestionsLenght = CheetosQuestions.length;
        var OtherQuestions = [2,3,4,5,6,7];
        var OtherQuestionsLenght = OtherQuestions.length;
        var SelectedQuestions = CreateEmptyIntList(3);
        var Answers = [1,0,0,0,1,0,1,0];
        var CurrentQuestion = 0;
        var Finish = false;
        var Init = false;



        var TypeList = ["Akbaş Çoban Köpeği","Jack Russell","Tekir","Golden Retreiver","Van Kedisi","Alman Kurdu","Border Collie","Sarman","Beagle","Maine Coon"];
        var TypelistLenght = TypeList.length;
        var ResultTexts = CreateEmptyStringList(TypeList.length);
        var ResultTextsAnim = CreateEmptyStringList(TypeList.length);
        var StringIndexes = CreateEmptyIntList(TypeList.length);
        var PercentList = SetPercentList(TypeList.length);
        var IndexListForStepByStepMode = CreateIndexList(TypeList.length);
        var IndexListForStepByStepModeLenght = IndexListForStepByStepMode.length;
        var IndexList = CreateIndexList(TypeList.length);
        var IndexListLenght =IndexList.length;
        var FilterDone = false;
        var ResultIndex = 0;

        //--------------------------------variables
        

        Diagnostics.log("Init");
        
            
        //update--------------------------------

        const recording = Time.setInterval(() => {

          if(!Init)
          {
            Init=true;
            for(let i=0;i<3;i++)
            {
                if(i==0)
                {
                  SelectedQuestions[i] = CheetosQuestions[Math.floor( Random.random()*CheetosQuestionsLenght)]
                }
                else{
                  SelectedQuestions[i] = OtherQuestions[Math.floor( Random.random()*OtherQuestionsLenght)]
                }
                Diagnostics.log("SelectedQuestions : "+SelectedQuestions);
                shuffle(SelectedQuestions);
                Diagnostics.log("SelectedQuestions : "+SelectedQuestions);

            }
          }


            //Diagnostics.log("randomPercent : " + (Math.floor( Random.random()* (101 - 75) + 75)));
            //Diagnostics.log("randomIndex : " + (Math.floor( Random.random()*(TypelistLenght+1))));
            if(isRecording.pinLastValue())
            {
                
                if(TimeIsUp.pinLastValue())
                {
                    if(!Finish)
                    {
                        Diagnostics.log("Finish");
                        Finish=true;

                        Diagnostics.log("SelectList : "+IndexList);
                        shuffle(IndexList);
                        Diagnostics.log("SelectList Shuffled : " +IndexList);
                        Diagnostics.log("PercentList : " + PercentList);

                        PercentList = BubbleSort(PercentList);

                        Diagnostics.log("PercentList + sort: " + PercentList);

                        //Patches.inputs.setScalar('ResultPercentNum',PercentList[0]);
                        SendPercentNum(PercentList[0]);
                        Patches.inputs.setScalar('ResultImageNum',IndexList[0]);

                        //Patches.inputs.setScalar('ResultPercentNum',IndexList[0]);
                        
                        //Patches.inputs.setScalar('ResultPercentNum',Math.floor( Random.random()* (101 - 75) + 75));
                        //Patches.inputs.setScalar('ResultImageNum',IndexList[0]);
                        
                        //SetResultTexts(ResultTexts.length);
                        //Diagnostics.log("ResultTexts : " +ResultTexts);
                        
                        //AllPatchesInputPushRandomOrNull(TypeList.length,false);
                    }
                    else
                    {
                        /*
                        if(DrawMode.pinLastValue()==0)
                        {
                        //Same Time Drawing Result Texts----------------------------------------------------------------

                        SetResultAnimTexts(TypeList.length);
                        
                        //----------------------------------------------------------------Same Time Drawing Result Texts
                        }
                        else if(DrawMode.pinLastValue()==1)
                        {
                        //Step by Step Drawing Result Texts----------------------------------------------------------------

                        if(IndexListForStepByStepModeLenght-1>=0) SetResultAnimTextsStepByStepMode((TypelistLenght-(IndexListForStepByStepModeLenght)));
                        
                        //----------------------------------------------------------------Step by Step Drawing Result Texts
                        }
                        */
                        
                    }
                }
                else
                {
                    //AllPatchesInputPushRandomOrNull(TypeList.length,true);
/*
                    if(RecordingTime.pinLastValue()> 5)
                    {
                        Diagnostics.log("RecordingTime : " + RecordingTime.pinLastValue());
                        Patches.inputs.setBoolean('ResultScreen',true);
                        Patches.inputs.setBoolean('RecordingScreen',false);
                    }
                    else{
                        Patches.inputs.setBoolean('ResultScreen',false);
                        Patches.inputs.setBoolean('RecordingScreen',true);
                        SendPercentNum(Math.floor( Random.random()*101));
                        //Patches.inputs.setScalar('ResultPercentNum',Math.floor( Random.random()*101));
                        Patches.inputs.setScalar('ResultImageNum',Math.floor( Random.random()*(TypelistLenght+1)));
                    }*/
                }
                 
            }            
        }, 10);  
        const recordingSlow = Time.setInterval(() => {
            
            if(Finish && !FilterDone)
            {
                Diagnostics.log("ResultIndex : " + ResultIndex);
                
                //Patches.inputs.setScalar('ResultPercentNum',PercentList[ResultIndex]);
                SendPercentNum(PercentList[ResultIndex]);
                Patches.inputs.setScalar('ResultImageNum',IndexList[ResultIndex]);
                
                ResultIndex++;

                if(ResultIndex>TypelistLenght-1)
                {
                    FilterDone=true;
                    Patches.inputs.setBoolean('FilterDone',true);
                }
            }
            
        }, 200);  
        const recordingMidSlow = Time.setInterval(() => {
            
            if(isRecording.pinLastValue())
            {
                if(!TimeIsUp.pinLastValue())
                {
                    if(RecordingTime.pinLastValue()> 5)
                    {
                        Diagnostics.log("RecordingTime : " + RecordingTime.pinLastValue());
                        Patches.inputs.setBoolean('ResultScreen',true);
                        Patches.inputs.setBoolean('RecordingScreen',false);
                    }
                    else{
                        Patches.inputs.setBoolean('ResultScreen',false);
                        Patches.inputs.setBoolean('RecordingScreen',true);
                        SendPercentNum(Math.floor( Random.random()*101));
                        //Patches.inputs.setScalar('ResultPercentNum',Math.floor( Random.random()*101));
                        Patches.inputs.setScalar('ResultImageNum',Math.floor( Random.random()*(TypelistLenght+1)));
                    }
                }
            }
        }, 100); 
        //--------------------------------update
        /*
        var a = x1 - x2;
        var b = y1 - y2;
        
        testRectangle.transform.y =recordVideoPoint.x.pinLastValue();
        var c = Math.sqrt( a*a + b*b );*/
        //otherFuncs--------------------------------------------
    function BubbleSort(arr)
    {

    for(let i = 0; i < arr.length; i++)
    {
            for(let j = 0; j < arr.length - i - 1; j++)
            {
                if(arr[j + 1] < arr[j])
                {
                    [arr[j + 1],arr[j]] = [arr[j],arr[j + 1]]
                }
            }
        };
        return arr;
    };
    function SendPercentNum(Num) 
    {
        if(Num==100)
        {
            Patches.inputs.setScalar('ResultPercent999',0);
            Patches.inputs.setScalar('ResultPercent99',0);
            Patches.inputs.setScalar('ResultPercent9',1);
        }
        else if(Num>=10 && Num <=99)
        {
            Patches.inputs.setScalar('ResultPercent999',-1);

            var mod10=Reactive.mod(Num,10);
            var div10=(Math.floor(Num/10));

            //var x = mod10.pinLastValue();

            Patches.inputs.setScalar('ResultPercent9',div10);
            Patches.inputs.setScalar('ResultPercent99',mod10);
        }
        else if(Num>=0 && Num<=9)
        {
            Patches.inputs.setScalar('ResultPercent999',-1);
            Patches.inputs.setScalar('ResultPercent99',-1);
            Patches.inputs.setScalar('ResultPercent9',Num);

        }
    }
    function MidPointBetweenTwoVec(vec1,vec2) 
    {
        var a = (vec1.x.pinLastValue() + vec2.x.pinLastValue() )/2;
        var b = (vec1.y.pinLastValue() + vec2.y.pinLastValue() )/2;
        var c = (vec1.z.pinLastValue() + vec2.z.pinLastValue() )/2;
        //var d = Reactive.vector(a,b,c);
        Patches.inputs.setVector('testvectorPos',Reactive.vector(a,b,c));

        
        var x = Reactive.point(point3.x,point3.y,point3.z);
        Diagnostics.log("vec2.x : " + x.x.pinLastValue());
        //planeLook.transform.rotation = planeLook.transform.lookAt(vec2).rotation;
        var k = planeLook.transform.lookAt(x).rotation.x;
        Diagnostics.log("k : " + k.pinLastValue());
        var l = planeLook.transform.lookAt(x).rotation.y;
        var m = planeLook.transform.lookAt(x).rotation.z;
        Patches.inputs.setVector('testvectorRot',Reactive.vector(k,l,m));
        //Diagnostics.log("planeLook.transform.rotation.x : " + Reactive.vector(k,l,m).x);
        //return d;
    }
    function DistanceBetweenTwoVec(vec1,vec2) 
    {
        var a = vec1.x.pinLastValue() - vec2.x.pinLastValue();
        var b = vec1.y.pinLastValue() - vec2.y.pinLastValue();
        var c = vec1.z.pinLastValue() - vec2.z.pinLastValue();
        var d = Math.sqrt( a*a + b*b +c*c);

        return d;
    }
    function SetPercentList(index) 
    {
        var resultArray=[];

        while (index != 0) {
            index--;
            if(index==0)
            {
                resultArray.push(Math.floor(Random.random()* (101 - 75) + 75));
            }
            else
                resultArray.push(Math.floor(Random.random()*75));

        }
        return resultArray;
    }
    function CreateEmptyStringList(index) 
    {
        var resultArray=[];

        while (index != 0) {
            index--;
            resultArray.push("");

        }
        return resultArray;
    }
    function CreateEmptyIntList(index) 
    {
        var resultArray=[];

        while (index != 0) {
            index--;
            resultArray.push(0);

        }
        return resultArray;
    }
    function SetResultAnimTextsStepByStepMode(index) 
    {
            if(StringIndexes[index]<ResultTexts[index].length)
            {
                ResultTextsAnim[index] = ResultTextsAnim[index] + ResultTexts[index][StringIndexes[index]];
                Patches.inputs.setString('ResultText_'+(index+1),ResultTextsAnim[index]);
                StringIndexes[index]= StringIndexes[index]+1;
            }
            else
            {
                IndexListForStepByStepMode.splice(0,1);
                IndexListForStepByStepModeLenght = IndexListForStepByStepMode.length;
            }
            
        
    }
    function SetResultAnimTexts(index) 
    {
        while (index != 0) {

            index--;
            if(StringIndexes[0]<ResultTexts[index].length)
            {
                ResultTextsAnim[index] = ResultTextsAnim[index] + ResultTexts[index][StringIndexes[0]];
                Patches.inputs.setString('ResultText_'+(index+1),ResultTextsAnim[index]);
            }

        }
        StringIndexes[0]= StringIndexes[0]+1;
    }
    function SetResultTexts(index) 
    {
        while (index != 0) {
            index--;

            ResultTexts[index] ="%"+Math.floor( Random.random()*101)+" " +TypeList[IndexList[IndexListLenght-1]];
            IndexList.pop();
            IndexListLenght = IndexList.length;

        }
    }
    function AllPatchesInputPushRandomOrNull(index,random) 
    {
        while (index != 0) {
            var pushString =""

            if(random) pushString ="%"+Math.floor( Random.random()*101)+" " +TypeList[Math.floor( Random.random()*6)];

            Patches.inputs.setString('ResultText_'+index,pushString);
            index--;
        }
    }
    function shuffle(array) 
    {
        let currentIndex = array.length,  randomIndex;
      
        while (currentIndex != 0) {
      
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex--;
      
          [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
        }
      
        return array;
    }
    function CreateIndexList(arrayLenght) 
    {
        let currentIndex = arrayLenght;
        var resultArray=[];
        while (currentIndex != 0) {
      
            currentIndex--;
            resultArray.push(currentIndex);
        }
      
        return resultArray;
    }

    //--------------------------------------------otherFuncs
    })
    
    
})();

/*

// Gets the position of an object as an R.point
// - Parameters:
//      e: The object to get the transform from
const getPosition = planeLook => R.point(planeLook.transform.x, planeLook.transform.y, planeLook.transform.z);

// Sets the rotation based on a transform
// - Parameters:
//      e: The object to rotate
//      p: The transform to use
const setRotation = (e, p) => {
  e.transform.rotationX = p.rotationX;
  e.transform.rotationY = p.rotationY;
  e.transform.rotationZ = p.rotationZ;
};

// Look at utility function.
// Because of reactive stupidness, we can't actually apply the lookat directly to the looker itself
// We get around this by nesting the looker object inside a null with no values applied to it's transform
//
// - Parameters:
//      _target: The object in the scene you want to face
//      _lookerParent: The parent object of the object you want to rotate. Should have no transform applied to it
//      _looker: The object that you want to rotate towards the target
const lookAt = (_target, _lookerParent, _looker) => {
  const ptToLookAt = getPosition(_target);
  const lookAtTransform = _lookerParent.transform.lookAt(ptToLookAt);
  setRotation(_looker, lookAtTransform);
};


// Random animation
const scl = R.val(0.1);
targetNull.transform.x = R.sin(Time.ms.mul(R.val(0.001))).mul(scl);
targetNull.transform.y = R.cos(Time.ms.mul(R.val(0.0007))).mul(scl);
targetNull.transform.z = R.sin(Time.ms.mul(R.val(0.0005))).mul(scl);

// Do the look at
lookAt(targetNull, followNull, plane);


*/ 

// Here's a simpler implementation without the helper functions
// const lookAtPt = R.point(targetNull.transform.x, targetNull.transform.y, targetNull.transform.z);
// const lookAtTransform = followNull.transform.lookAt(lookAtPt);

// plane.transform.rotationX = lookAtTransform.rotationX;
// plane.transform.rotationY = lookAtTransform.rotationY;
// plane.transform.rotationZ = lookAtTransform.rotationZ;

/* 
BACKUP OLD LINES-------------------

    let recordVideoPoint = await Patches.outputs.getVector('recordVideoPoint');

//Diagnostics.log("ceil :" + Math.ceil(num) );

Patches.inputs.setScalar('Time',time);
            
testRectangle.transform.y =recordVideoPoint.x.pinLastValue();
            
if(index<(stringFullLength))
{
    string = string + stringFull[index];
    Patches.inputs.setString('AnimTextTest',string);
    Diagnostics.log(string);
    Diagnostics.log("(stringFullLength-1)"+(stringFullLength));
    Diagnostics.log("index"+index);
    index= index+1;
}

Patches.inputs.setString('ResultText_1',"%"+Math.floor( Random.random()*101)+" " +TypeList[Math.floor( Random.random()*6)]);
Patches.inputs.setString('ResultText_2',"%"+Math.floor( Random.random()*101)+" " +TypeList[Math.floor( Random.random()*6)]);
Patches.inputs.setString('ResultText_3',"%"+Math.floor( Random.random()*101)+" " +TypeList[Math.floor( Random.random()*6)]);
Patches.inputs.setString('ResultText_4',"%"+Math.floor( Random.random()*101)+" " +TypeList[Math.floor( Random.random()*6)]);
Patches.inputs.setString('ResultText_5',"%"+Math.floor( Random.random()*101)+" " +TypeList[Math.floor( Random.random()*6)]);
Patches.inputs.setString('ResultText_6',"%"+Math.floor( Random.random()*101)+" " +TypeList[Math.floor( Random.random()*6)]);

//Patches.inputs.setScalar('RandomNumber6',Math.floor( Random.random()*101));
//Patches.inputs.setString('myString',""+Math.floor( Random.random()*101));


Diagnostics.log("SelectList : "+SelectList);
shuffle(SelectList);
Diagnostics.log("SelectList Shuffled : " +SelectList);
SelectList.pop();
Diagnostics.log("SelectList Shuffled pop : " +SelectList);
SelectList.pop();
Diagnostics.log("SelectList Shuffled pop2 : " +SelectList);
                        
        var ResultText1="";
        var ResultText1Anim="";
        var ResultText2="";
        var ResultText2Anim="";
        var ResultText3="";
        var ResultText3Anim="";
        var ResultText4="";
        var ResultText4Anim="";
        var ResultText5="";
        var ResultText5Anim="";
        var ResultText6="";
        var ResultText6Anim="";
        
        var StringIndex1 = 0;
        var StringIndex2 = 0;
        var StringIndex3 = 0;
        var StringIndex4 = 0;
        var StringIndex5 = 0;
        var StringIndex6 = 0;
        
                        ResultText1 = "%"+Math.floor( Random.random()*101)+" " +TypeList[IndexList[IndexListLenght-1]];
                        IndexList.pop();
                        IndexListLenght = IndexList.length;
                        ResultText2 = "%"+Math.floor( Random.random()*101)+" " +TypeList[IndexList[IndexListLenght-1]];
                        IndexList.pop();
                        IndexListLenght = IndexList.length;
                        ResultText3 = "%"+Math.floor( Random.random()*101)+" " +TypeList[IndexList[IndexListLenght-1]];
                        IndexList.pop();
                        IndexListLenght = IndexList.length;
                        ResultText4 = "%"+Math.floor( Random.random()*101)+" " +TypeList[IndexList[IndexListLenght-1]];
                        IndexList.pop();
                        IndexListLenght = IndexList.length;
                        ResultText5 = "%"+Math.floor( Random.random()*101)+" " +TypeList[IndexList[IndexListLenght-1]];
                        IndexList.pop();
                        IndexListLenght = IndexList.length;
                        ResultText6 = "%"+Math.floor( Random.random()*101)+" " +TypeList[IndexList[IndexListLenght-1]];
                        
//Same Time Drawing Result Texts----------------------------------------------------------------
                        
                        if(StringIndex<ResultText1.length)
                        {
                            ResultText1Anim = ResultText1Anim + ResultText1[StringIndex];
                            Patches.inputs.setString('ResultText_1',ResultText1Anim);
                        }
                        if(StringIndex<ResultText2.length)
                        {
                            ResultText2Anim = ResultText2Anim + ResultText2[StringIndex];
                            Patches.inputs.setString('ResultText_2',ResultText2Anim);
                        }
                        if(StringIndex<ResultText3.length)
                        {
                            ResultText3Anim = ResultText3Anim + ResultText3[StringIndex];
                            Patches.inputs.setString('ResultText_3',ResultText3Anim);
                        }
                        if(StringIndex<ResultText4.length)
                        {
                            ResultText4Anim = ResultText4Anim + ResultText4[StringIndex];
                            Patches.inputs.setString('ResultText_4',ResultText4Anim);
                        }
                        if(StringIndex<ResultText5.length)
                        {
                            ResultText5Anim = ResultText5Anim + ResultText5[StringIndex];
                            Patches.inputs.setString('ResultText_5',ResultText5Anim);
                        }
                        if(StringIndex<ResultText6.length)
                        {
                            ResultText6Anim = ResultText6Anim + ResultText6[StringIndex];
                            Patches.inputs.setString('ResultText_6',ResultText6Anim);
                        }
                        
                        //----------------------------------------------------------------Same Time Drawing Result Texts
                        
 
                        //Step by Step Drawing Result Texts----------------------------------------------------------------
                        if(StringIndex1<ResultText1.length)
                        {
                            ResultText1Anim = ResultText1Anim + ResultText1[StringIndex1];
                            Patches.inputs.setString('ResultText_1',ResultText1Anim);
                            StringIndex1= StringIndex1+1;
                        }
                        else if(StringIndex2<ResultText2.length)
                        {
                            ResultText2Anim = ResultText2Anim + ResultText2[StringIndex2];
                            Patches.inputs.setString('ResultText_2',ResultText2Anim);
                            StringIndex2= StringIndex2+1;
                        }
                        else if(StringIndex3<ResultText3.length)
                        {
                            ResultText3Anim = ResultText3Anim + ResultText3[StringIndex3];
                            Patches.inputs.setString('ResultText_3',ResultText3Anim);
                            StringIndex3= StringIndex3+1;
                        }
                        else if(StringIndex4<ResultText4.length)
                        {
                            ResultText4Anim = ResultText4Anim + ResultText4[StringIndex4];
                            Patches.inputs.setString('ResultText_4',ResultText4Anim);
                            StringIndex4= StringIndex4+1;
                        }
                        else if(StringIndex5<ResultText5.length)
                        {
                            ResultText5Anim = ResultText5Anim + ResultText5[StringIndex5];
                            Patches.inputs.setString('ResultText_5',ResultText5Anim);
                            StringIndex5= StringIndex5+1;
                        }
                        else if(StringIndex6<ResultText6.length)
                        {
                            ResultText6Anim = ResultText6Anim + ResultText6[StringIndex6];
                            Patches.inputs.setString('ResultText_6',ResultText6Anim);
                            StringIndex6= StringIndex6+1;
                        }
                        //----------------------------------------------------------------Step by Step Drawing Result Texts
                        //Step by Step Drawing Result Texts----------------------------------------------------------------
                        if(StringIndex1<ResultText1.length)
                        {
                            ResultText1Anim = ResultText1Anim + ResultText1[StringIndex1];
                            Patches.inputs.setString('ResultText_1',ResultText1Anim);
                            StringIndex1= StringIndex1+1;
                        }
                        else if(StringIndex2<ResultText2.length)
                        {
                            ResultText2Anim = ResultText2Anim + ResultText2[StringIndex2];
                            Patches.inputs.setString('ResultText_2',ResultText2Anim);
                            StringIndex2= StringIndex2+1;
                        }
                        else if(StringIndex3<ResultText3.length)
                        {
                            ResultText3Anim = ResultText3Anim + ResultText3[StringIndex3];
                            Patches.inputs.setString('ResultText_3',ResultText3Anim);
                            StringIndex3= StringIndex3+1;
                        }
                        else if(StringIndex4<ResultText4.length)
                        {
                            ResultText4Anim = ResultText4Anim + ResultText4[StringIndex4];
                            Patches.inputs.setString('ResultText_4',ResultText4Anim);
                            StringIndex4= StringIndex4+1;
                        }
                        else if(StringIndex5<ResultText5.length)
                        {
                            ResultText5Anim = ResultText5Anim + ResultText5[StringIndex5];
                            Patches.inputs.setString('ResultText_5',ResultText5Anim);
                            StringIndex5= StringIndex5+1;
                        }
                        else if(StringIndex6<ResultText6.length)
                        {
                            ResultText6Anim = ResultText6Anim + ResultText6[StringIndex6];
                            Patches.inputs.setString('ResultText_6',ResultText6Anim);
                            StringIndex6= StringIndex6+1;
                        }
                        //----------------------------------------------------------------Step by Step Drawing Result Texts

-------------------BACKUP OLD LINES
*/